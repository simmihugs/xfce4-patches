# xfce4-patches

## Patch for xfce4-terminal

creates 4 rightclick option

## How patch was created

```
git diff 26e0e5039299b45b1d00ff625ed719a3c1562613 > rightclick-copy-to-cliboard-or-paste.patch
```

## How to apply patch

```
git apply rightclick-copy-to-cliboard-or-paste.patch
```
